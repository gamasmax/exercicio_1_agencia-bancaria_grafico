/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariagrafica;

import static java.lang.Integer.parseInt;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    private Pessoa[] pessoas = new Pessoa[10];
    private Conta[] contas = new Conta[10];
    @FXML
    private TextField nome;
    @FXML
    private TextField endereco;
    @FXML
    private TextField cidade;
    @FXML
    private TextField bairro;
    @FXML
    private TextField estado;
    @FXML
    private TextField fone;
    @FXML
    private TextField email;
    @FXML
    private TextField cpf;
    @FXML
    private TextField rg;
    @FXML
    private TextField titular;
    @FXML
    private TextField numero;
    @FXML
    private TextField digito;
    @FXML
    private TextField saldo;
    @FXML
    private Label saldoText;
    @FXML
    private Label erroConta;
    @FXML
    private Label erroPessoa;
    @FXML
    private TextField pessoaNome;
    @FXML
    private TextField numeroConta;
    @FXML
    private TextField valor;

    
    int i=0;
    int c=0;
    @FXML
    private void adicionarPessoa(ActionEvent event) {
        if (i>8){
            erroPessoa.setText("ERRO NUMERO MAXIMO DE PESSOAS ATINGIDO");
        }
        else{
        if(i!=0){
          boolean pessoaIgual=false;
          for (int j=0; j<i; j++){
              if (pessoas[j].getNome().equals(nome.getText())){
                  pessoaIgual=true;
              }
          }
          if (pessoaIgual==false){
              pessoas[i] = new Pessoa();
              pessoas[i].setNome(nome.getText());
              pessoas[i].setEndereco(endereco.getText());
              pessoas[i].setCidade(cidade.getText());
              pessoas[i].setBairro(bairro.getText());
              pessoas[i].setEstado(estado.getText());
              pessoas[i].setFone(parseInt(fone.getText()));
              pessoas[i].setEmail(email.getText());
              pessoas[i].setCpf(cpf.getText());
              pessoas[i].setRg(rg.getText());
              i++;
              erroPessoa.setText("Conta criada com sucesso");
              nome.setText("");
              endereco.setText("");
              cidade.setText("");
              bairro.setText("");
              estado.setText("");
              fone.setText("");
              email.setText("");
              cpf.setText("");
              rg.setText("");
              
          }
          else{
              erroPessoa.setText("ERRO NOME REPETIDO");
          }
        }
        else{
        pessoas[i] = new Pessoa();
        pessoas[i].setNome(nome.getText());
        pessoas[i].setEndereco(endereco.getText());
        pessoas[i].setCidade(cidade.getText());
        pessoas[i].setBairro(bairro.getText());
        pessoas[i].setEstado(estado.getText());
        pessoas[i].setFone(parseInt(fone.getText()));
        pessoas[i].setEmail(email.getText());
        pessoas[i].setCpf(cpf.getText());
        pessoas[i].setRg(rg.getText());
        i++;
         erroPessoa.setText("Conta criada com sucesso");
            nome.setText("");
            endereco.setText("");
            cidade.setText("");
            bairro.setText("");
            estado.setText("");
            fone.setText("");
            email.setText("");
            cpf.setText("");
            rg.setText("");
        }
    }
    }
    @FXML
    private void adicionarConta(ActionEvent event){
        boolean achouPessoa=false;
        if (c>8){
            erroConta.setText("ERRO NUMERO MAXIMO DE CONTAS ATINGIDO"); 
        }
        else{
        for (int j=0; j<i; j++){
            if (pessoas[j].getNome().equals(titular.getText())){
                
                achouPessoa=true;
                contas[c] = new Conta();
                contas[c].setTitular(pessoas[j]);
                contas[c].setDigito(Integer.parseInt(digito.getText()));
                contas[c].setNumero(Integer.parseInt(numero.getText()));
                contas[c].setSaldo(Integer.parseInt(saldo.getText()));
                erroConta.setText("Conta de " + pessoas[j].getNome() + ", com numero " + numero.getText() + " foi criada com sucesso");
                c++;            
                digito.setText("");
                numero.setText("");
                saldo.setText("");

            }
        }
        if (achouPessoa==false){
            erroConta.setText("ERRO nenhuma pessoa com esse nome foi achado");
        }
    }
    }
    @FXML
    private void debita(ActionEvent event){
    boolean achou=false;
      for (int j=0; j<c; j++){
          if (contas[j].getTitular().getNome().equals(pessoaNome.getText())&&contas[j].getNumero()==Integer.parseInt(numeroConta.getText())){
            achou=true;
            if(contas[j].debita(Float.parseFloat(valor.getText()))){
                saldoText.setText("Debitado com sucesso");
            }
            else{
                saldoText.setText("Falhar ao debitar");
            }
        }
      }
      if (!achou){
          saldoText.setText("Erro conta não encontrada");
      }  
    
    }
    @FXML
    private void credita(ActionEvent event){
        boolean achou=false;
      for (int j=0; j<c; j++){
          if (contas[j].getTitular().getNome().equals(pessoaNome.getText())&&contas[j].getNumero()==Integer.parseInt(numeroConta.getText())){
            achou=true;
            contas[j].credita(Float.parseFloat(valor.getText()));
            saldoText.setText("R$ " + valor.getText() + " creditado com sucesso, saldo atual: " + contas[j].getSaldo());
          }
      }
      if (!achou){
          saldoText.setText("Erro conta não encontrada");
      }
        
    }
    
    @FXML
    private void verSaldo(ActionEvent event){
      boolean achou=false;
      for (int j=0; j<c; j++){
          if (contas[j].getTitular().getNome().equals(pessoaNome.getText())&&contas[j].getNumero()==Integer.parseInt(numeroConta.getText())){
            achou=true;
            saldoText.setText("Saldo R$ " + contas[j].getSaldo());
          }
      }
      if (!achou){
          saldoText.setText("Erro conta não encontrada");
      }
      
      
        
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
