/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariagrafica;

public class Conta {
   private Pessoa titular;
   private int numero;
   private int digito;
   private float saldo;
   
   public Conta(Pessoa titular, int numero, int digito, float saldo){
    this.titular=titular;
    this.numero=numero;
    this.digito=digito;
    this.saldo=saldo;
   }
   public Conta(){
       
   }
   public boolean debita(float valor){
       if (valor>this.saldo){
           return false;
       }
       else{
           this.saldo-=valor;
           return true;
       }
   }
   public void credita (float valor){
       this.saldo+=valor;
   }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public Pessoa getTitular() {
        return titular;
    }

    public void setTitular(Pessoa titular) {
        this.titular = titular;
    }

}
